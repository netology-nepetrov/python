# Управляющие конструкции и типы данных. Часть 1

### типы переменных
- integer - целые числа. int()
- string - строка, текст. str()
- float - числа с плавающей точкой. float()
- boolean - булевый, логический тип. bool()
- list - список.
- type() - узнать тип данных

### операции со строками
- `+` - конкатенация(обхединение)
- `.capitalize()` - приводит первую букву к верхнему регистру
- `* int` - повторить строку несколько раз
- `.replace('что заменить','на что заменить')`
- `.upper()` - привести к верхнему регистру
- `.lower()`- привести к нижнему регистру
- `len(my_string)` - узнать количество символов в строке
- получать значение элемента можно через []. причем можно указать условно отрицательный индекс -6 с конца
- `my_string[0]` - нулевой символ
- `my_string[0:5]` - с нулевого по пятый(не включая 5)
- `my_string[6:]` - с шестого символа
- `my_string[2:8:2]` - с второго по восьмой(не включая 8), с шагом 2
- `if word in my_string: print(word)` - если слово есть в строке, то вывести это слово.
- `print('Hello {}, i know {} a bit'.format(name, lang))` - подставить переменные name и lang вместо {}.
- `print(f'Hello {name}, i know {lang} a bit')` - подставить переменные name и lang в вывод.
- `print('Hello ' + str(name) + ', i know ' + str(lang) + ' a bit')` - подставить переменные name и lang в вывод. при этом включены операторы, которые преобразуют в строку значение переменной.

### Операции со списками

- `list = list1 + list2` - сложить списки
- `list[0][1] = 123` - сменить значение элемента списка.
- `list[0:2] = [['key1': value1], ['key2': value2]]` - заменить первые 2 значения в списке `list`.
- `a, b = [1, 2]` - указать значения сразу двух переменных
- `del(list[index])` - удаляет элемент из списка по индексу
- `.remove(el)` - удаляет указанный жлемент из списка
- `append(el)` - добавляет жлемент в список
- `.count(el)` - считает количество вхождений элемента в список
- `.index(el)` - позволяет узнать индекс жлемента в списке
- `.reverse()` - разворачивает список
- `sorted(list)` - сортирует список
- `first, second, third = ['первый', 'второй', 'третий']`- задать 3 переменные
- `first, *_ = ['первый', 'второй', 'третий']` - 'first' = 'первый', '_' = ['второй', 'третий'] - таким образом остальные значения попали в переменную '_'.
- `first, *other, last = ['первый', 'второй', 'третий', 'четвертый']` - 'first' = 'первый', 'other' = ['второй', 'третий'], 'last' = 'четвертый'
- `income_by_months[0:2] = [['yan', 1000], ['feb', 2000]]` - так можно переопределить первые 2 элемента списка.
- `income_by_months = income_by_months + income_by_months_2` - сложить списки
- `del(income_by_months[-1])` - удалить последний элемент списка
- `month_list.remove('Sep')` - удалить элемент по значению
- `income_by_months.append(['dec', 1000])` - добавить элемент в конец списка
- `income_list.insert(2, 1000)` - добавить по индексу, второй элемент с значением 1000
- `income_list.count(13000)` - посчитать сколько вхождений 13000 есть в списке
- `income_list.index(13000)` - найти индекс первого вхождения
- `income_list.index(13000, 1)` - найти индекс второго вхождения
- `income_list.reverse()` - обратный порядок элементов в списке
- `len(income_list)` - длинна списка
- `sum(income_list)` - сумма элементов если они int
- `max(income_list)` - максимальный элемент
- `min(income_list)` - минимальный элемент
- `sorted(income_list)` - сортировка списка
- `sorted(income_list, reverse= True)` - порядок сортировки обратный
- `id(a)` - посмотреть айдишник элемента
- `import copy` - импортировать библиотеку copy
- `b = copy.copy(a)` - с помощью библиотеки copy скопировали значение переменной a и поместили в b.
- `b = copy.deepcopy(a)` - тоже самое, только копирование с вложенностью.
- `queries_list = queries_string.split(',')` - так можно из строки сделать список, указав разделитель
- `print(','.join(['st1', 'st2', 'st3']))` - поставить разделитель из кавычек, перез join в списке
- `salary_tuple = (100, 200, 300)` - создали кортеж. дело в круглых скобках. кортеж не изменяемый. (tuple) - кортеж
- `salaries_by_names = zip(names, salaries)` - охединение попарно двух списков
- `list(salaries_by_names)` - посмотреть содержимое

## циклы
- `while x<5:` выполняется пока условие true
- `for element in list:` - выполняется пока есть элементы в списке
- `break` - закончить выполнение и остановить цикл
- `continue` - продолжить работу цикла, не продолжая обрабатывать команды ниже, вернувшись в начало цикла
- `pass` - пропустить строку и продолжить работать дальше
- `print(string, end=' ')` - выводить в строку, пока не дойдем до пробела
- `range` - диапазон. занимает мало памяти, потому что в один момент времени в памяти одно число.

тут будет вывод цифр с 0 до 9
```
for i in range(10):
    print(i)
```
мы преобразовываем в список, тут будет вывод с 2 до 10 с шагом 5
```
list(range(2, 10, 5))
```
каждому элементу присвоить номер

```
list(enumerate([1, 2, 3, 4]))
[(0, 1), (1, 2), (2, 3), (3, 4)]
```

это используется например, если нужно пронумеровать список
```
companies_capitalization = [
    ['Orange', 1.3],
    ['Maxisoft', 1.5],
    ['Headbook', 0.8],
    ['Nicola', 2.2]
]

for i, company in enumerate(companies_capitalization):
    print(i+1, company[0], 'capitalization is', company[1])

# вывод

companies_capitalization = [
    ['Orange', 1.3],
    ['Maxisoft', 1.5],
    ['Headbook', 0.8],
    ['Nicola', 2.2]
]

for i, company in enumerate(companies_capitalization):
    print(i+1, company[0], 'capitalization is', company[1])
output
1 Orange capitalization is 1.3
2 Maxisoft capitalization is 1.5
3 Headbook capitalization is 0.8
4 Nicola capitalization is 2.2
```

пример задачи:  

```
cook_book = [
  ['салат',
      [
        ['картофель', 100, 'гр.'],
        ['морковь', 50, 'гр.'],
        ['огурцы', 50, 'гр.'],
        ['горошек', 30, 'гр.'],
        ['майонез', 70, 'мл.'],
      ]
  ],
  ['пицца',  
      [
        ['сыр', 50, 'гр.'],
        ['томаты', 50, 'гр.'],
        ['тесто', 100, 'гр.'],
        ['бекон', 30, 'гр.'],
        ['колбаса', 30, 'гр.'],
        ['грибы', 20, 'гр.'],
      ],
  ],
  ['фруктовый десерт',
      [
        ['хурма', 60, 'гр.'],
        ['киви', 60, 'гр.'],
        ['творог', 60, 'гр.'],
        ['сахар', 10, 'гр.'],
        ['мед', 50, 'мл.'],  
      ]
  ]
]

```
Необходимо вывести пользователю список покупок необходимого количества ингредиентов для приготовления блюд на определенное число персон в следующем виде:  

```
for item in cook_book:
    print(item[0].capitalize())
    
    for line in item[1]:
        meal, weight, gr = line
        weight = weight * person
        
        print(meal, weight, gr)
    
    print('')
```

```
# Дана последовательность чисел. Мы хотим оставить только те, что делятся на 5
sequence = range(0, 40, 3)
list(sequence)
```

```
# решение в лоб
for num in sequence:
    if num % 5 == 0:
        print(num)
```

```
# если хотим получить отфильтрованный лист, то будет даже так
filtered_sequence = []

for num in sequence:
    if num % 5 == 0:
        filtered_sequence.append(num)

print(filtered_sequence)
```

```
# тоже самое, но в одну строчку
[num for num in sequence if num % 5 == 0]
```

```
#слева преобразование полученного значения, справа после if фильтр
[num*2 for num in sequence if num % 5 == 0]
```

множества (set), это те же списки, только с уникальными значениями, без повторений и в случайном порядке. поиск по ним будет быстрее

Набор неповторяющихся элементов в случайном порядке
```
data_scientist_skills = set(['Python', 'R', 'SQL', 'Tableau', 'SAS', 'Git'])
data_engineer_skills = set(['Python', 'Java', 'Scala', 'Git', 'SQL', 'Hadoop'])
```

#### логическое ИЛИ – что нужно знать data-scientst, который по совместительству data-engineer

```
print(data_scientist_skills.union(data_engineer_skills))
print(data_scientist_skills | data_engineer_skills)
```

#### логическое И – что нужно знать и data-scientist и data-engineer

```
print(data_scientist_skills.intersection(data_engineer_skills))
print(data_scientist_skills & data_engineer_skills)

```
#### разность множеств – что знает data-scientist, но не знает data-engineer (и наоборот)
#### print(data_scientist_skills.difference(data_engineer_skills))
#### print(data_scientist_skills - data_engineer_skills)
```
print(data_engineer_skills.difference(data_scientist_skills))
print(data_engineer_skills - data_scientist_skills)
```

#### симметричная разность множеств – что такого знают data-scientist и data-engineer, чего не знают они оба
#### print(data_scientist_skills.symmetric_difference(data_engineer_skills))
#### print(data_scientist_skills ^ data_engineer_skills)
```
print(data_engineer_skills.symmetric_difference(data_scientist_skills))
print(data_engineer_skills ^ data_scientist_skills)
```

## словари

```
salaries = {
    'John': 1200,
    'Mary': 500,
    'Steven': 1000,
    'Liza': 1500
}
```

#### обращение к элементу словаря
```
salaries['John']
```

#### удаляем элемент из словаря
```
del(salaries['Liza'])
salaries
```

#### добавляем элемент в словарь
```
salaries['Mary'] = 2000
salaries
```

#### безопасно получаем значение по ключу, если его нет, то выводим сообщение
```
# salaries['Oleg']
# print(salaries.get('Oleg', 'Not found'))
salaries.get('Mary', 'Not Found')
```

#### проверка на наличие ключа в словаре
```
recruit = 'Amanda'

if recruit in salaries:
    print('Значение для ключа уже существует')
else:
    print('Добавляю новый ключ')
    salaries[recruit] = 2200

print(salaries)
```

#### Можно использовать метод setdefault
#### setdefault не изменяет значение, если ключ уже был в словаре

```
# salaries.setdefault('Mary', 3000)
# salaries
salaries.setdefault('Paul', 3000)
salaries
```

#### перейдем к более сложному словарю
```
staff_dict = {
    'Robert': {'salary': 800, 'bonus': 200}, 
    'Jane': {'salary': 200, 'bonus': 300}, 
    'Liza': {'salary': 1300, 'bonus': 200}, 
    'Richard': {'salary': 500, 'bonus': 1200}
}
```

#### обращение к элементу словаря
```
staff_dict['Robert']['bonus']
```

#### добавление элемента в словарь
```
staff_dict['Oleg'] = {'salary': 1000000, 'bonus': 300}
staff_dict
```

# получаем только ключи/значения из словаря (очень пригодиться в циклах)
```
print(staff_dict.keys())
print(staff_dict.values())
print(staff_dict.items())

print(list(staff_dict.keys()))
print(list(staff_dict.values()))
print(list(staff_dict.items()))
```

#### функция zip
```
categories = ['Еда', 'Авто', 'Политика', '346346']
audience = [100, 200, 300]

dict(zip(categories, audience))

# categories_dict = dict(zip(categories, audience))
# print(categories_dict)
```

#### как быстро создать словарь
```
{n: n**2 for n in range(10)}
```

## работа с файлами

```
# открываем файл для чтения (опция r)
f = open('visit_log.csv', 'r')

# прочитать построчно
f.readline()

# прочитать все содержимое файла в переменную content
content = f.readlines()

# построчное чтение файла
for line in f:
    print(line)
    
    break # остановить после первой итерации


# удаляем перенос строки и лишние пробелы
another_line.strip()

# разбиваем столбцы
another_line.strip().split(',')

# закрытие файла
f.close()


# прочитать все строчки файла в список (т. е. в оперативную память)
f = open('visit_log.csv', 'r')
content = f.readlines()
f.close()

# зашита от записи
with open('results.csv', 'w') as f:
    f.write('Начинаю запись первой строки...\n')
    f.write('Начинаю запись второй строки...\n')
    
    # в данном случае запись не удастся
    my_friend_results = open('results.csv', 'w')
    my_friend_results.write('Тут еще результаты есть')
    my_friend_results.close()


# сразу читать и писать в файлы
with open('visit_log.csv', 'r') as f:
    with open('visit_contexts.csv', 'w') as f2write:
        for line in f:
            if context in line:
                f2write.write(line)


# вывести первые 5 строк
with open('purchase_log.txt') as f:
    print([next(f) for x in range(5)])

```
- работа с json

```
import json

# перевод строки в словарь
dict_in_string = '{"a": 1, "b": 2}'
type(json.loads(dict_in_string))


# перевод строки в список
list_in_string = '[1, 2, 3]'
json.loads(list_in_string)[-1]



# чтение и приведение в json
i = 0
with open('purchase_log.txt') as f:
    for line in f:
        line = line.strip()
        
        dict_ = json.loads(line)
        print(dict_, type(dict_))
        
        i += 1
        if i > 5:
            break

# когда мы моработали с json, потом привести обратно в строки для записи в файл
data = {'user_id': '1840e0b9d4', 'category': 'Продукты'}
json.dumps(data)
type(json.dumps(data))

# Запись объекта сразу в файл как поток байтов
import pickle
data = {'user_id': '1840e0b9d4', 'category': 'Продукты'}
with open('data.pickle', 'wb') as f:
    pickle.dump(data, f)

# прочитать объект из такого файла
with open('data.pickle', 'rb') as f:
    dict_ = pickle.load(f)
dict_, dict_['user_id']


import os
# чтение файлов и папок
for file in os.listdir('data'):
    print(file)

# чтение всех файлов и папок, в том числе вложенных
for root, directory, file in os.walk('data'):
    print(root, directory, file)

```

## работа с yaml

```
import yaml
data = yaml.load(YAML_STR, Loader=yaml.FullLoader)

# вывод загруженных данных
#print(data)
import pprint
pprint.pprint(data)

# Итерировать книги
for item in data:
    print(item["book"]["id"])

# Итерировать книги, поиск по цене (поиск в качестве строки)
for item in data:
    if "36" in str(item["book"]["price"]):
        print(item["book"]["id"])

# Итерировать книги, поиск по цене (поиск в качестве числа)
for item in data:
    if 6.95 == item["book"]["price"]:
        print(item["book"]["id"])

# Сохранить в виде строки
from io import StringIO
output = StringIO()
yaml.dump(data, output, default_flow_style=False)
yaml_string = output.getvalue()
print(yaml_string)

```

# работа с xml
```
import lxml.html as LH

root = LH.fromstring(XML_STR)

# Пройти по элементам
for child in root:
    print(child.tag, child.attrib)
    print(child[0].tag, child[0].text)

from lxml import etree
xml_string = etree.tostring(root)

import pprint
pprint.pprint(xml_string)
```
